clear all
close all
clc

addpath('./Radio');

% --- Radio parameters 
carrierFreq = 930e6;
bandwidth   = 8e6;
rxGain      = 25;


% --- Socket and radio config
obj = pyUDPsocket(6789,'');
e310_setConfigRadio('192.168.10.11','Rx','freq',carrierFreq);
e310_setConfigRadio('192.168.10.11','Rx','samp_rate',bandwidth);
e310_setConfigRadio('192.168.10.11','Rx','gain',rxGain);

% --- Acqusition parameters
tgSize  = 2048;
nbMean  = 24;
xAx     = ((0:tgSize-1)/tgSize - 0.5)*bandwidth;
figure;
while true
    sF = 0;
    for iN = 1 : 1 : nbMean
        % --- Get data
        sigRx = getDATA_UDP(obj,2048);
        % --- FFT
        sF    = sF + 1/nbMean*abs(fftshift(fft(sigRx)));
    end
    plot(xAx,20*log10(sF));
    grid 
    ylim([-60 0]);
    pause(0.01);
end

close(obj);


