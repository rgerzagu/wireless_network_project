clear all
close all
clc

addpath('./Radio/');
% --- Radio parameters 
carrierFreq = 930e6;
bandwidth   = 8e6;
rxGain      = 25;


% --- Socket and radio config
obj = pyUDPsocket(6789,'');
e310_setConfigRadio('192.168.10.11','Rx','freq',carrierFreq);
e310_setConfigRadio('192.168.10.11','Rx','samp_rate',bandwidth);
e310_setConfigRadio('192.168.10.11','Rx','gain',rxGain);

% --- Getting data
tgSize = 2048;
sigRx = getDATA_UDP(obj,tgSize);

% --- Close socket
close(obj);


