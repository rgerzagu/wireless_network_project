function [ output ] = symbol_demapping(symb_in,channel_in,config )
if isstruct(config)
    modulation_nb_bit   = config.ModulationNbBit;
else
    modulation_nb_bit = config;
end
if(modulation_nb_bit == 1) %BPSK
    output = (symb_in).*abs(channel_in).^2;
else
    if(modulation_nb_bit == 2) %QPSK
        output = zeros(1,length(symb_in)*2);
        output(1:2:end) = real(symb_in).*abs(channel_in).^2;
        output(2:2:end) = imag(symb_in).*abs(channel_in).^2;
    else
       if(modulation_nb_bit == 4) %16 QAM
           alpha = 1/sqrt(10);
           %max log approximation
           % -------------------------%
           %   |      |     |     |
           %  10     11    01    00
           %  -3     -1     1     3      
           output = zeros(1,length(symb_in)*4);
           [m n] = size(symb_in);
           symb_in_tmp = symb_in;
           channel_in_tmp = channel_in;
           if(n<m)
              clear symb_in_tmp;
              symb_in_tmp = symb_in.';
              clear channel_in_tmp;
              channel_in_tmp = channel_in.';
           end
           
           s = real(symb_in_tmp);           
           output(1:4:end) = (max([2*s-alpha;6*s-9*alpha])-max([-2*s-alpha;-6*s-9*alpha])).*abs(channel_in_tmp).^2;
           output(2:4:end) = (max([6*s-9*alpha;-6*s-9*alpha])-max([-2*s-alpha;2*s-alpha])).*abs(channel_in_tmp).^2;
                     
           s = imag(symb_in_tmp);  
           output(3:4:end) = (max([2*s-alpha;6*s-9*alpha])-max([-2*s-alpha;-6*s-9*alpha])).*abs(channel_in_tmp).^2;
           output(4:4:end) = (max([6*s-9*alpha;-6*s-9*alpha])-max([-2*s-alpha;2*s-alpha])).*abs(channel_in_tmp).^2;
                   

       else
           if(modulation_nb_bit == 6) %64 QAM
                alpha = 1/sqrt(42);
                %max log approximation
               output = zeros(1,length(symb_in)*6);
               [m n] = size(symb_in);
               symb_in_tmp = symb_in;
               channel_in_tmp = channel_in;
               if(n<m)
                  clear symb_in_tmp;
                  symb_in_tmp = symb_in.';
                  clear channel_in_tmp;
                  channel_in_tmp = channel_in.';
               end

               s = real(symb_in_tmp); 
               output(1:6:end) = (max([ 2*s- 1*alpha; 6*s- 9*alpha; 10*s-25*alpha; 14*s-49*alpha]) - max([ -2*s- 1*alpha; -6*s- 9*alpha;-10*s-25*alpha;-14*s-49*alpha])).*abs(channel_in_tmp).^2;
               output(2:6:end) = (max([14*s-49*alpha;10*s-25*alpha;-14*s-49*alpha;-10*s-25*alpha]) - max([  6*s- 9*alpha;  2*s- 1*alpha; -6*s- 9*alpha; -2*s- 1*alpha])).*abs(channel_in_tmp).^2;
               output(3:6:end) = (max([14*s-49*alpha; 2*s- 1*alpha;-14*s-49*alpha; -2*s- 1*alpha]) - max([  6*s- 9*alpha; 10*s-25*alpha; -6*s- 9*alpha;-10*s-25*alpha])).*abs(channel_in_tmp).^2;
               
               s = imag(symb_in_tmp); 
               output(4:6:end) = (max([ 2*s- 1*alpha; 6*s- 9*alpha; 10*s-25*alpha; 14*s-49*alpha]) - max([ -2*s- 1*alpha; -6*s- 9*alpha;-10*s-25*alpha;-14*s-49*alpha])).*abs(channel_in_tmp).^2;
               output(5:6:end) = (max([14*s-49*alpha;10*s-25*alpha;-14*s-49*alpha;-10*s-25*alpha]) - max([  6*s- 9*alpha;  2*s- 1*alpha; -6*s- 9*alpha; -2*s- 1*alpha])).*abs(channel_in_tmp).^2;
               output(6:6:end) = (max([14*s-49*alpha; 2*s- 1*alpha;-14*s-49*alpha; -2*s- 1*alpha]) - max([  6*s- 9*alpha; 10*s-25*alpha; -6*s- 9*alpha;-10*s-25*alpha])).*abs(channel_in_tmp).^2;

           else
%             error('modulation parameter %d not supported\n\n',modulation_nb_bit);  
                [m n] = size(symb_in);
                if(m>n)
                    symb_in = symb_in.';
                    channel_in = channel_in.';
                end
                
                L = length(symb_in);
                alpha = [-15:2:15]./sqrt(170);
                dist_re = zeros(16,L);
                dist_im = zeros(16,L);
                for idx=1:16
                    dist_re(idx,:) = -abs(real(symb_in)-alpha(idx)).^2;
                    dist_im(idx,:) = -abs(imag(symb_in)-alpha(idx)).^2;
                end
                tab_index_max_bit_0 = zeros(4,8);
                tab_index_max_bit_1 = zeros(4,8);
                
                tab_index_max_bit_0(1,:) = [9:16];
                tab_index_max_bit_1(1,:) = [1:8];
                tab_index_max_bit_0(2,:) = [  1  2  3  4 13 14 15 16];
                tab_index_max_bit_1(2,:) = [  5  6  7  8  9 10 11 12];
                tab_index_max_bit_0(3,:) = [  1  2  7  8  9 10 15 16];
                tab_index_max_bit_1(3,:) = [  3  4  5  6 11 12 13 14];
                tab_index_max_bit_0(4,:) = [  1  4  5  8  9 12 13 16];
                tab_index_max_bit_1(4,:) = [  2  3  6  7 10 11 14 15];

                output_tmp = zeros(8,L);

                for idx=1:4
                   llr_re = max(dist_re(tab_index_max_bit_0(idx,:),:))-max(dist_re(tab_index_max_bit_1(idx,:),:));
                   llr_im = max(dist_im(tab_index_max_bit_0(idx,:),:))-max(dist_im(tab_index_max_bit_1(idx,:),:));
                   output_tmp(idx  ,:) = llr_re;
                   output_tmp(idx+4,:) = llr_im;   
                end
                 
                output_tmp = output_tmp.*kron(abs(channel_in).^2,ones(8,1));

                output = zeros(1,numel(output_tmp));
                for idx=1:8
                    output(idx:8:end) = output_tmp(idx,:);
                end


           end            
       end
    end
end 

end

